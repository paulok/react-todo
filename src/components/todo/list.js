import React, {Component} from 'react';
import TodoListItem from './list_item';

const TodoList = ({todos}) => {
    const todoItems = todos.map((todo) => {
        return <TodoListItem key={todo.token} todo={todo} />
    });

    return (
        <div className="row justify-content-center">
            <div className='todo-container col-md-6'>
                <div className='todo-list-title'>ToDo List</div>
                <ul className=' list-group'>
                    { todoItems }
                </ul>
            </div>
        </div>
        
    );
};

export default TodoList;