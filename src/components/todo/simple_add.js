import React, { Component } from 'react';

class SimpleAdd extends Component {
    constructor(props){
        super(props);

        this.state = {title: ''};

        //Binds
        this.onInputChange = this.onInputChange.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
    }

    componentDidMount() {
        //Call ajax to load current ToDo list
    }

    onInputChange(event){
        this.setState({ title: event.target.value });
    }

    onFormSubmit(event) {
        event.preventDefault();
        /**Prevent empty data */
        if(this.state.title.trim() == '') return false;

        /**Call onFormSubmit from App Component (into index.js) */
        let randtoken  = require('rand-token');
        let token = randtoken.generate(8);

        this.props.onFormSubmit({token ,title: this.state.title});
        this.setState({title: ''});
    }

    render() {
        return (
            <form onSubmit={ this.onFormSubmit } className="input-group col-mb-12" id="formTodoSimpleAdd">
                <input 
                    type="text" 
                    value={this.state.title} 
                    className='form-control'
                    onChange={ this.onInputChange }
                    placeholder="What do you have to do?" 
                    name="title"
                />
                <div className="input-group-append">
                    <button type="submit" form="formTodoSimpleAdd" className="btn btn-outline-secondary">
                        <span className="glyphicon glyphicon-plus"></span>Add
                    </button>
                </div>
            </form>
        );
    }

}

export default SimpleAdd;