import React from 'react';

const TodoListItem = ({todo}) => {
    return (
        <li className='list-group-item' id={"todo-item-"+todo.token}>
            {todo.title}
        </li>
    );
};

export default TodoListItem;