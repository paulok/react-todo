import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import TodoSimpleAdd from './components/todo/simple_add';
import TodoList from './components/todo/list';

class App extends Component{
    constructor(props){
        super(props);

        this.handleFormSubmitTodoSimpleAdd = this.handleFormSubmitTodoSimpleAdd.bind(this);
        this.state = { todos: [] };
    }

    handleFormSubmitTodoSimpleAdd(todo){
        let todos = this.state.todos;
        todos.unshift(todo);
        this.setState({todos});
    }

    render(){
        return (
            <div>
                <TodoSimpleAdd onFormSubmit={this.handleFormSubmitTodoSimpleAdd} />
                <TodoList todos={this.state.todos} />
            </div>
        );
    }
}

// Take this component's generated HTML and put it on the page
ReactDOM.render(<App />, document.querySelector('.container'));
